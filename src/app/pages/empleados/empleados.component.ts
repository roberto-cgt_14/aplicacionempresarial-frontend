import { EmpleadoI } from "./../../models/empleado.interface";
import { ApiService } from "./../../services/api/api.service";
import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from "@angular/forms";

@Component({
  selector: "app-empleados",
  templateUrl: "./empleados.component.html",
  styleUrls: ["./empleados.component.css"],
})
export class EmpleadosComponent implements OnInit {
  empleados: EmpleadoI[];
  selEmpleado: number;
  idxEmpleado: number;
  datoEmpleado: EmpleadoI;

  nuevoForm = new FormGroup({
    nombre: new FormControl("", Validators.required),
    brm: new FormControl("", Validators.required),
    puesto: new FormControl("Selecciona un puesto..", Validators.required),
    foto: new FormControl("", Validators.required),
  });

  editarForm = new FormGroup({
    id: new FormControl(""),
    nombre: new FormControl("", Validators.required),
    brm: new FormControl("", Validators.required),
    puesto: new FormControl("", Validators.required),
    foto: new FormControl("", Validators.required),
  });
  constructor(private api: ApiService) {}

  ngOnInit() {
    this.obtenerEmpleados();
  }

  postForm(form: EmpleadoI) {
    const formData = new FormData();
    formData.append("brm", this.nuevoForm.get("brm").value);
    formData.append("nombre", this.nuevoForm.get("nombre").value);
    formData.append("puesto", this.nuevoForm.get("puesto").value);
    formData.append("foto", this.nuevoForm.get("foto").value);
    console.log(formData);

    this.api.postEmpleado(formData).subscribe((data) => {
      if (data) {
        this.obtenerEmpleados();
        this.nuevoForm.reset();
      }
    });
  }

  putForm() {
    const formData = new FormData();

    formData.append("brm", this.editarForm.get("brm").value);
    formData.append("nombre", this.editarForm.get("nombre").value);
    formData.append("puesto", this.editarForm.get("puesto").value);
    formData.append("foto", this.editarForm.get("foto").value);

    this.api.putEmpleado(this.editarForm.get("id").value, formData).subscribe(
      (data) => {
        if (data) {
          console.log(data);
          this.obtenerEmpleados();
          this.nuevoForm.reset();
        }
      },
      (err) => {
        if (err.statusText === "OK") {
          this.obtenerEmpleados();
        }
      }
    );
  }

  onChangeFoto(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.nuevoForm.get("foto").setValue(file);
    }
  }
  onChangeFotoEditar(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.editarForm.get("foto").setValue(file);
    }
  }

  obtenerEmpleados() {
    this.api
      .getAllEmpleados()
      .subscribe((data: EmpleadoI[]) => (this.empleados = data));
  }

  eliminarEmpleado() {
    this.api.deleteEmpleado(this.selEmpleado).subscribe(
      (data) => console.log(data),
      (err) => {
        if (err.statusText === "OK") {
          this.empleados.splice(this.idxEmpleado, 1);
        }
      }
    );
  }

  selectEmpleado(id, idx) {
    this.selEmpleado = id;
    this.idxEmpleado = idx;
  }
  selectEditar(id) {
    this.api.getEmpleado(id).subscribe((data) =>
      this.editarForm.setValue({
        id: id,
        nombre: data.nombre,
        brm: data.brm,
        puesto: data.puesto,
        foto: data.foto,
      })
    );
  }
}
