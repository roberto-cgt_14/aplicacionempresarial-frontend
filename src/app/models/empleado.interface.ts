export interface EmpleadoI {
  id: number;
  brm: string;
  nombre: string;
  puesto: string;
  foto: any;
}
