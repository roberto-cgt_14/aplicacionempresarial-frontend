import { ResponseI } from "./../../models/response.interface";
import { EmpleadoI } from "./../../models/empleado.interface";
import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getAllEmpleados(): Observable<EmpleadoI[]> {
    return this.http.get<EmpleadoI[]>(
      "https://app-aplicacion-empresarial.herokuapp.com/api/empleados"
    );
  }

  deleteEmpleado(id): Observable<ResponseI> {
    return this.http.delete<ResponseI>(
      "https://app-aplicacion-empresarial.herokuapp.com/api/empleados/" + id
    );
  }

  postEmpleado(form) {
    return this.http.post<ResponseI>(
      "https://app-aplicacion-empresarial.herokuapp.com/api/empleados",
      form
    );
  }
  putEmpleado(id, form) {
    return this.http.put<ResponseI>(
      "https://app-aplicacion-empresarial.herokuapp.com/api/empleados/" + id,
      form
    );
  }

  getEmpleado(id) {
    return this.http.get<EmpleadoI>(
      "https://app-aplicacion-empresarial.herokuapp.com/api/empleados/" + id
    );
  }
}
